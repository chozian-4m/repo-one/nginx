ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8
ARG BASE_TAG=8.5

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG} as build

RUN dnf upgrade -y --nodocs && \
    dnf install -y --nodocs \
       gcc \
       make \
       openssl-devel \
       pcre-devel \
       perl \
       zlib-devel && \
    dnf clean all && \
    rm -rf /var/cache/dnf

COPY nginx.tar.gz ngx_devel_kit.tar.gz set-misc-nginx-module.tar.gz /

RUN mkdir -p /usr/local/src/{nginx,ngx_devel_kit,set-misc-nginx-module} && \
    tar -zxf /nginx.tar.gz --strip-components=1 -C /usr/local/src/nginx && \
    tar -zxf /ngx_devel_kit.tar.gz --strip-components=1 -C /usr/local/src/ngx_devel_kit && \
    tar -zxf /set-misc-nginx-module.tar.gz --strip-components=1 -C /usr/local/src/set-misc-nginx-module && \
    cd /usr/local/src/nginx && \
    ./configure --prefix=/etc/nginx \
       --sbin-path=/usr/sbin/nginx \
       --modules-path=/usr/lib64/nginx/modules \
       --conf-path=/etc/nginx/nginx.conf \
       --error-log-path=/var/log/nginx/error.log \
       --http-log-path=/var/log/nginx/access.log \
       --pid-path=/var/run/nginx.pid \
       --lock-path=/var/run/nginx.lock \
       --http-client-body-temp-path=/var/cache/nginx/client_temp \
       --http-proxy-temp-path=/var/cache/nginx/proxy_temp \
       --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp \
       --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp \
       --http-scgi-temp-path=/var/cache/nginx/scgi_temp \
       --user=nginx \
       --group=nginx \
       --with-compat \
       --with-file-aio \
       --with-threads \
       --with-http_addition_module \
       --with-http_auth_request_module \
       --with-http_dav_module \
       --with-http_flv_module \
       --with-http_gunzip_module \
       --with-http_gzip_static_module \
       --with-http_mp4_module \
       --with-http_random_index_module \
       --with-http_realip_module \
       --with-http_secure_link_module \
       --with-http_slice_module \
       --with-http_ssl_module \
       --with-http_stub_status_module \
       --with-http_sub_module \
       --with-http_v2_module \
       --with-mail \
       --with-mail_ssl_module \
       --with-stream \
       --with-stream_realip_module \
       --with-stream_ssl_module \
       --with-stream_ssl_preread_module \
       --with-cc-opt='-O2 -g -pipe -Wall -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -Wp,-D_GLIBCXX_ASSERTIONS -fexceptions -fstack-protector-strong -grecord-gcc-switches -specs=/usr/lib/rpm/redhat/redhat-hardened-cc1 -specs=/usr/lib/rpm/redhat/redhat-annobin-cc1 -m64 -mtune=generic -fasynchronous-unwind-tables -fstack-clash-protection -fcf-protection -fPIC' \
       --with-ld-opt='-Wl,-z,relro -Wl,-z,now -pie' \
       --add-dynamic-module=/usr/local/src/ngx_devel_kit \
       --add-dynamic-module=/usr/local/src/set-misc-nginx-module && \
     make && \
     make install

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

USER root

COPY nginx_signing.key nginx.rpm /tmp/
COPY scripts/docker-entrypoint.sh /docker-entrypoint.sh
COPY conf/nginx.conf /etc/nginx/nginx.conf

RUN dnf upgrade -y && \
    rpm --import /tmp/nginx_signing.key && \
    dnf install -y /tmp/nginx.rpm gettext && \
    rm /tmp/nginx.rpm && \

    # Create necessary directories
    mkdir -p /docker-entrypoint.d/ && \
    mkdir -p /etc/nginx/templates && \

    # Fix nginx user permissions (user auto-created during rpm installation)
    touch /var/cache/nginx/nginx.pid && \
    chown -R nginx:nginx /var/cache/nginx && \
    chown -R nginx:nginx /var/log/nginx && \
    chown -R nginx:nginx /etc/nginx && \
    chown -R nginx:nginx /var/cache/nginx/nginx.pid && \
    chown -R nginx:nginx /docker-entrypoint.d && \

    # Cleanup installation
    dnf clean all && \
    rm -rf /var/cache/dnf && \

    # Forward nginx logs to stdout and stderr
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log

RUN cp -r /usr/share/nginx/html /etc/nginx && \
    rm /etc/nginx/conf.d/default.conf

COPY --from=build /usr/lib64/nginx/modules /usr/lib64/nginx/modules
COPY scripts/10-listen-on-ipv6-by-default.sh scripts/20-envsubst-on-templates.sh scripts/30-tune-worker-processes.sh /docker-entrypoint.d/
RUN chown -R nginx:nginx /docker-entrypoint.d && \
    chmod o-w etc/nginx/nginx.conf && \
    chmod o-w docker-entrypoint.d/10-listen-on-ipv6-by-default.sh && \
    chmod o-w docker-entrypoint.d/30-tune-worker-processes.sh && \
    chmod o-w docker-entrypoint.d/20-envsubst-on-templates.sh && \
    chmod o-w docker-entrypoint.sh

USER nginx

EXPOSE 8080 8443

HEALTHCHECK --interval=10s --timeout=5s --start-period=1m --retries=5 \
   CMD curl -I -f --max-time 5 http://localhost:8080 || curl -fsk https://localhost:8443 || exit 1

ENTRYPOINT ["/docker-entrypoint.sh"]

STOPSIGNAL SIGQUIT

CMD ["nginx", "-g", "daemon off;"]
